using Literate

OUTPUT = joinpath(@__DIR__, "generated/")

retirement_example = joinpath(@__DIR__, "retirement_example.jl")
Literate.markdown(retirement_example, OUTPUT; credit=false, documenter=true)
Literate.notebook(retirement_example, OUTPUT; credit=false)

budgeting_example = joinpath(@__DIR__, "budgeting_example.jl")
Literate.markdown(budgeting_example, OUTPUT; credit=false, documenter=true)
Literate.notebook(budgeting_example, OUTPUT; credit=false)

mortgage_example = joinpath(@__DIR__, "mortgage_example.jl")
Literate.markdown(mortgage_example, OUTPUT; credit=false, documenter=true)
Literate.notebook(mortgage_example, OUTPUT; credit=false)