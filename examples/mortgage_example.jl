using PersonalFinance
const pf = PersonalFinance

# Set up your mortgage information
principal = 400000
down = 0.2
annual_rate = 0.03
term = Dates.Year(30)
M = pf.FixedRateMortgage(principal, down, annual_rate, term)

# Set up your estimated property taxes and insurance (taken from Redfin)
property_tax_rate = 0.0105
insurance_rate = 0.0022
H = pf.HousePayment(M, property_tax_rate, insurance_rate)

# Grab some plots
results = pf.calculateMonthlyPayments(H)
pf.plotMonthlyPayments(results, display_plots=false)
pf.summary(results)