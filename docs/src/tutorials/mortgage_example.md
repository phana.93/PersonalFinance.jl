# Mortgage Calculation
```@example mortgage_example
using PersonalFinance
const pf = PersonalFinance
nothing; # hide
```

Set up your mortgage information:
```@example mortgage_example
principal = 400000
down = 0.2
annual_rate = 0.03
term = Dates.Year(30)
M = pf.FixedRateMortgage(principal, down, annual_rate, term)
nothing; # hide
```

Set up your estimated property taxes and insurance:
```@example mortgage_example
property_tax_rate = 0.0105
insurance_rate = 0.0022
H = pf.HousePayment(M, property_tax_rate, insurance_rate)
nothing; # hide
```

Grab some plots:
```@example mortgage_example
results = pf.calculateMonthlyPayments(H)
p = pf.plotMonthlyPayments(results)
savefig(p["Monthly Payments"], "monthly.svg"); nothing # hide
savefig(p["Amortization Schedule"], "schedule.svg"); nothing # hide
pf.summary(results)
```
```@raw html
<img src="../monthly.svg" width="200%"/>
<img src="../schedule.svg" width="200%"/>
``` ⠀