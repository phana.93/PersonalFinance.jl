# Budgeting
```@example budgeting_example
using PersonalFinance
const pf = PersonalFinance
nothing; # hide
```

Set up your salary information (this example is based on median salaries in California).
```@example budgeting_example
salary_growth_rate = 0.03
pay_period = Dates.Week(2)
start_date = Dates.Date(2019, 1, 1)
salary = pf.SimpleSalary(72000.0, salary_growth_rate, pay_period, start_date)
nothing; # hide
```

For budgeting over the short-term, please considers your:
* your bank accounts
* your recurring expenses
* your one-time expenses
```@example budgeting_example
accounts = Dict{String,pf.BankAccount}()
accounts["Checking"] = pf.BankAccount(1000, 0.001, Dates.Year(1))
accounts["Savings"] = pf.BankAccount(5000, 0.001, Dates.Year(1))

expenses = Dict{String,pf.RecurringExpense}()
expenses["Rent"] = pf.RecurringExpense(1400, Dates.Month(1), Dates.Date(2018, 5, 5))
expenses["Food"] = pf.RecurringExpense(30, Dates.Day(1), start_date)
expenses["Netflix"] = pf.RecurringExpense(10, Dates.Month(1), Dates.Date(2017, 9, 17))
expenses["Internet"] = pf.RecurringExpense(50, Dates.Month(1), Dates.Date(2018, 5, 7))
expenses["Phone"] = pf.RecurringExpense(55, Dates.Month(1), Dates.Date(2014, 2, 14))
expenses["Gas"] = pf.RecurringExpense(55, Dates.Week(1), Dates.Date(2019, 2, 1))

expenses["Headphones"] = pf.SingleExpense(150, start_date + Dates.Day(5))
nothing; # hide
```

Create a simple portfolio and simulate it.
```@example budgeting_example
portfolio = pf.SimplePortfolio(salary, accounts, expenses)
out = pf.simulateBudget(portfolio, start_date = start_date, duration = Dates.Month(5))
nothing; # hide
```

Finally, let's generate some plots.
```@example budgeting_example
p0 = pf.plot(out)
p1 = pf.plotpie(pf.averageRecurringExpensesOverOneYear(expenses, start_date), "Average Recurring Expenses per Month (US\$)")
savefig(p0, "budget_planning.svg"); nothing # hide
savefig(p1, "expenses.svg"); nothing # hide
nothing; # hide
```
```@raw html
<img src="../budget_planning.svg" width="200%"/>
<img src="../expenses.svg" width="200%"/>
``` ⠀