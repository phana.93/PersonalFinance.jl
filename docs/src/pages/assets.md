# Assets & Liabilities

```@index
Pages   = ["assets.md"]
```
```@meta
DocTestSetup = quote
    using PersonalFinance
end
```

## Assets
```@docs
PersonalFinance.BankAccount
PersonalFinance.SimpleRetirementAsset
```
## Expenses
```@docs
PersonalFinance.RecurringExpense
PersonalFinance.SingleExpense
PersonalFinance.getValuePerDay
PersonalFinance.estimateAnnualExpenses
```