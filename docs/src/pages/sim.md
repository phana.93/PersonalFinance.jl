# Simulation

```@index
Pages = ["sim.md"]
```
```@meta
DocTestSetup = quote
    using PersonalFinance
end
```

```@docs
PersonalFinance.PersonalProfile
PersonalFinance.SimplePortfolio
PersonalFinance.RetirementPortfolio
PersonalFinance.simulateBudget
PersonalFinance.SimReturns
PersonalFinance.simulateRetirement
PersonalFinance.RetirementSimOutput
```