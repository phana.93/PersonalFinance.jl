# Plots

```@index
Pages   = ["plots.md"]
```
```@meta
DocTestSetup = quote
    using PersonalFinance
end
```

```@docs
PersonalFinance.plotRetirement
PersonalFinance.plotFixedRateMortgage
PersonalFinance.plotMonthlyPayments
```
