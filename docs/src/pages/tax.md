# Taxes

```@index
Pages   = ["tax.md"]
```
```@meta
DocTestSetup = quote
    using PersonalFinance
end
```

## Usage
!!! warn
    `FilingStatus` is not handled fully throughout this package yet -- until further notice, assume everything is filed as 'single'.

```@docs
PersonalFinance.FilingStatus
PersonalFinance.hasTax
PersonalFinance.getValidTaxConfiguration
PersonalFinance.calcTax
```
## Federal Tax
```@docs
PersonalFinance.hasFederalTax
PersonalFinance.calcFederalTax
PersonalFinance.calcSSTax
PersonalFinance.calcMedicareTax
PersonalFinance.calcFICATax
```
## State Tax
```@docs
PersonalFinance.hasStateTax
PersonalFinance.calcStateTax
```