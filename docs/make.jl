using Documenter, PersonalFinance

makedocs(format=Documenter.HTML(),
         sitename="PersonalFinance.jl",
         pages=["Home" => "index.md",
                  "Tutorials" => ["tutorials/budgeting.md",
                                  "tutorials/mortgage_example.md",
                                  "tutorials/retirement_planning.md"],
                  "API" => ["pages/assets.md",
                            "pages/mortgage.md",
                            "pages/salary.md",
                            "pages/tax.md",
                            "pages/sim.md",
                            "pages/plots.md"],
                  ],
         repo="https://gitlab.com/kylecarbon/PersonalFinance.jl/blob/{commit}{path}#{line}")
