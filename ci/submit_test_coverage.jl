
include("init.jl")

cd(Pkg.dir("PersonalFinance"))

Pkg.add("Coverage")
using Coverage
Codecov.submit_local(process_folder())
