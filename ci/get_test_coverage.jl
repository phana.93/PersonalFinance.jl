include("init.jl")

Pkg.test("PersonalFinance"; coverage=true)

Pkg.add("Coverage")
Pkg.update()

cd(Pkg.dir("PersonalFinance"))

using Coverage, PersonalFinance
coverage = process_folder()
covered_lines, total_lines = get_summary(coverage)
percentage = covered_lines / total_lines * 100.0
println("($(percentage)%) covered")
if isinteractive()
    clean_folder(PersonalFinance.dir())
end
