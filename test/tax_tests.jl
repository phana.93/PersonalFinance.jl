
@testset "Tax availability" begin
    state, year = pf.getValidTaxConfiguration()
    @test pf.hasTax(state, year) == true
end