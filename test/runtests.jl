using Test

using PersonalFinance
const pf = PersonalFinance

pf.greet()
@time @testset "Federal Tax Tests" begin include("federal_tax_tests.jl") end
@time @testset "State Tax Tests" begin include("state_tax_tests.jl") end
@time @testset "Tax Tests" begin include("tax_tests.jl") end
@time @testset "Salary Tests" begin include("salary_tests.jl") end
@time @testset "Profile Tests" begin include("profile_tests.jl") end
@time @testset "Expenses Tests" begin include("expenses_tests.jl") end
@time @testset "Asset Tests" begin include("asset_tests.jl") end
@time @testset "Helper Functions Tests" begin include("helper_functions_tests.jl") end
@time @testset "Sim Tests" begin include("sim_tests.jl") end
@time @testset "Examples Tests" begin include("example_tests.jl") end
@time @testset "Mortgage Tests" begin include("mortgage_tests.jl") end
