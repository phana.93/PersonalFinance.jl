# checked against: https://www.irscalculators.com/tax-calculator
@testset "California Tax" begin
    @test pf.calcStateTax(100000.0, "CA", 2018) ≈ 6144.142
    @test pf.calcStateTax(200000.0, "CA", 2018) ≈ 15444.142
    @test pf.calcStateTax(473000, "CA", 2019) ≈ 43606.417
    @test pf.calcStateTax(1000, "CA", 2019) == 0.0

    @test pf.hasStateTax("CA", 2018) == true
    @test pf.hasStateTax("CA", 2019) == true
    @test pf.hasStateTax("CA", 1800) == false

    @test_throws ArgumentError pf.calcStateTax(100000.0, "CA", 1800)
    @test_throws ArgumentError pf.calcStateTax(100000.0, "WA", 2019)
end
