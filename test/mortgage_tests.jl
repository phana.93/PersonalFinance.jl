@testset "Mortgage Calculations" begin
    @testset "Fixed Year Calculations" begin
        r = 0.065
        d = 0
        N = Dates.Month(30 * 12)
        P = 200000.0
        monthly_payment, total_cost = pf.calculateFixedRateMortgagePayment(P, d, r, N)
        @test round(monthly_payment, digits=2) ≈ 1264.14

        M = pf.FixedRateMortgage(P, d, r, N)
        c, _ = pf.calculateFixedRateMortgagePayment(M)
        @test c == monthly_payment
    end
    @testset "Amortization Schedule: 0% down payment" begin
        # Tested against: https://www.bankrate.com/calculators/mortgages/amortization-calculator.aspx
        r = 0.04
        d = 0
        N = Dates.Year(30)
        P = 200000.0
        M = pf.FixedRateMortgage(P, d, r, N)

        out = pf.calculateFixedRateMortgageSchedule(M)
        @test round(out.total_cost, digits=2) == 343739.01
        @test round(out.total_interest, digits=2) == 143739.01
        @test round(out.monthly_payment, digits=2) == 954.83
        @test round(out.principal_payments[1], digits=2) == 288.16
        @test round(out.principal_payments[6], digits=2) == 293.00
        @test round(out.principal_payments[end], digits=2) == 951.66
        @test round(out.interest_payments[1], digits=2) == 666.67
        @test round(out.interest_payments[6], digits=2) == 661.83
        @test round(out.interest_payments[end], digits=2) == 3.17
        @test out.balance_remaining[end] == 0.0
    end
    @testset "Amortization Schedule: 20% down payment" begin
        # Tested against: https://www.bankrate.com/calculators/mortgages/amortization-calculator.aspx
        r = 0.04
        d = 0.2
        N = Dates.Year(30)
        P = 200000.0
        M = pf.FixedRateMortgage(P, d, r, N)

        out = pf.calculateFixedRateMortgageSchedule(M)
        @test round(out.total_cost, digits=2) == 314991.21
        @test round(out.total_interest, digits=2) == 114991.21
        @test round(out.monthly_payment, digits=2) == 763.86
        @test round(out.principal_payments[1], digits=2) == 230.53
        @test round(out.principal_payments[6], digits=2) == 234.4
        @test round(out.principal_payments[end], digits=2) == 761.33
        @test round(out.interest_payments[1], digits=2) == 533.33
        @test round(out.interest_payments[6], digits=2) == 529.47
        @test round(out.interest_payments[end], digits=2) == 2.54
        @test round(out.balance_remaining[5], digits=2) == 158839.63
        @test out.balance_remaining[end] == 0.0
    end
end
@testset "Full House Payment" begin
    r = 0.04
    d = 0.2
    N = Dates.Year(30)
    P = 200000.0
    M = pf.FixedRateMortgage(P, d, r, N)

    H = pf.HousePayment(M, .01, .0022)
    out = pf.calculateMonthlyPayments(H)

    pf.summary(out)
end