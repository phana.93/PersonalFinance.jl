@testset "SimpleRetirementAsset" begin
    Random.seed!(0)
    start_date = Dates.Date(2019, 1, 1)
    principal = 1000
    # I feel bad about this....
    rate = 0.1
    norm = Distributions.Normal(rate, 0.000000000001)
    asset = pf.SimpleRetirementAsset(norm, principal, start_date)
    @testset "Sanity check" begin
        num_years = 5
        end_date = start_date + Dates.Year(num_years)
        dates, values = pf.getValuePerDay(asset, start_date, end_date)
        @test length(dates) == length(values)
        @test values[end] ≈ principal * (1 + rate)^num_years
    end
    @testset "Slicing" begin
        num_years = 3
        dates, values = pf.getValuePerDay(asset, start_date + Dates.Year(1), start_date + Dates.Year(num_years))
        @test length(dates) == length(values)
        @test values[1] ≈ principal * (1 + rate)
        @test values[end] ≈ principal * (1 + rate)^num_years
    end
end
