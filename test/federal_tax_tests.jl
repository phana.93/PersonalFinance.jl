# checked against: https://www.irscalculators.com/tax-calculator
# backup reference: https://smartasset.com/taxes/income-taxes#TLAlYQZFTg
@testset "Federal Single Tax" begin
    @test pf.calcFederalTax(100000.0, 2018) ≈ 15409.5
    @test pf.calcFederalTax(100000.0, 2019) ≈ 15246.5
    @test pf.calcFederalTax(200000, 2019) ≈ 41412.5
    @test pf.calcFederalTax(14000, 2019) ≈ 180
    @test pf.calcFederalTax(10000, 2019) == 0.0
    @test pf.calcFederalTax(10000, 2020) == 0.0
    @test pf.calcFederalTax(1400000, 2020) ≈ 477839.0

    @test pf.hasFederalTax(2019) == true
    @test pf.hasFederalTax(2018) == true
    @test pf.hasFederalTax(1800) == false

    # no info for the year 2200
    @test_throws ArgumentError pf.calcFederalTax(100000.0, 2200)
end

@testset "FICA Tax" begin
    @test pf.calcSSTax(100000, 2019) ≈ 6200.0
    @test pf.calcSSTax(200000, 2019) ≈ 8239.8
    @test pf.calcMedicareTax(100000, 2019) ≈ 1450.0

    @test pf.calcFICATax(473000.0, 2019) ≈ 17555.3
    @test_throws ArgumentError pf.calcSSTax(100000.0, 1800)
    @test_throws ArgumentError pf.calcMedicareTax(100000.0, 1800)
end
