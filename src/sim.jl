#= 
Copyright [2020] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#

"""
    SimReturns

Sorry for the name...Simple struct to wrap:
* `dates::Vector{Date}`
* `returns::Vector{Float64}`
"""
mutable struct SimReturns
    dates::Vector{Date}
    returns::Vector{Float64}
end

SimReturns() = SimReturns(Vector{Date}(undef, 0), Vector{Float64}(undef, 0))

"""
    simulateBudget(portfolio::SimplePortfolio; duration = Dates.Week(12), start_date = Dates.today(), state = "CA")

Simulate your assets & liabilities on a DAILY basis over the next `duration`. Returns a [`SimReturns`](@ref) as output.
This only simulates highly liquid vehicles, e.g. [`BankAccount`](@ref) and [`RecurringExpense`](@ref)s.
This is meant to help visualise your short-term cash position.
"""
function simulateBudget(portfolio::SimplePortfolio; duration=Dates.Week(12), start_date=Dates.today(), state="CA")
    if !validateExpenseToAssetMapping(portfolio.expenses2assets, keys(portfolio.expenses), keys(portfolio.accounts))
        error("Incorrect mapping of expenses to assets. Please double-check $(portfolio.expenses2assets).")
    end
    end_date = start_date + duration
    sim_dates = start_date:Dates.Day(1):end_date

    # calculate all recurring expenses and their dates of occurrence
    deltas = Vector{Pair{Vector{Date},Vector{Float64}}}()
    for (expense_name, expense) ∈ portfolio.expenses
        dates, expenses = getValuePerDay(expense, start_date, end_date)
        push!(deltas, Pair(dates, expenses))
    end

    # estimate salary, adjusted for taxes (federal, FICA, state)
    dates, paychecks = getSalaryPerDay(portfolio.salary, start_date, end_date,
                                        estimate_tax=true,
                                        state=state)
    push!(deltas, Pair(dates, paychecks))

    sim_out = SimReturns()
    sim_out.dates = sim_dates
    returns = zeros(length(sim_dates))
    returns[1] = sumAssets(portfolio)

    for (index, date) ∈ enumerate(sim_dates)
        if index == 1
            prev_value = returns[index]
        else
            prev_value = returns[index - 1]
        end
        value = prev_value
        for (days, values) ∈ deltas
            for (delta_ind, day) ∈ enumerate(days)
                if day == date
                    value += values[delta_ind]
                end
            end
        end
        returns[index] = value
    end

    sim_out.returns = returns
    return sim_out
end

function simulateBudget(portfolio::RetirementPortfolio; duration=Dates.Week(12), start_date=Dates.today(), state="CA")
    new_portfolio = SimplePortfolio(portfolio.salary, portfolio.accounts, portfolio.expenses)
    return simulateBudget(new_portfolio, duration=duration, start_date=start_date, state=state)
end

"""
    RetirementSimOutput

A struct which contains:
* `portfolio`: the input [`RetirementPortfolio`](@ref) used as an input
* `start_date::Date`: the starting date for the simulation
* `salary::Vector{Float64}`: salary as a function of time
* `expenses::Dict{String, Vector{Float64}}`: each expense is tracked as a function of time
* `savings::Vector{Float64}`: expected savings as a function of time
* `assets::Dict{String,Matrix{Float64}}`: per asset, output a matrix of results, where rows correspond to number of years, columns are individual samples from the Monte Carlo simulation
* `liabilities::Dict{String,Vector{Float64}}`: per liability, the value as a function of time
* `net_worth::Matrix{Float64}`: net worth as a function of time. Rows correspond to number of years from `start_date`, whereas columns are samples from the Monte Carlo simulation.
"""
mutable struct RetirementSimOutput
    portfolio::RetirementPortfolio
    start_date::Date
    salary::Vector{Float64}
    expenses::Dict{String,Vector{Float64}}
    savings::Vector{Float64}
    assets::Dict{String,Matrix{Float64}}
    liabilities::Dict{String,Vector{Float64}}
    net_worth::Matrix{Float64}
end

"""
    simulateRetirement(portfolio::RetirementPortfolio; start_date = Dates.today(), samples = 10000)

Run a simple Monte Carlo simulation of your [`RetirementPortfolio`](@ref) on an annual basis to help plan for your retirement.
Assumptions include:
* `samples` are used to sample from [`SimpleRetirementAsset`](@ref)
* Expenses are estimated on an annual basis via [`estimateAnnualExpenses`](@ref)
* Salary is also estimated on an annual basis (TODO: document this)
    * Taxes are calculated using the most recent tax year
* Leftover funds after accounting for expenses and taxes are split equally among all retirement assets in the portfolio

TODOs:
* allow for a mapping of savings to different retirement assets
* account for different types of retirement accounts (e.g. traditional vs. Roth accounts)
* calculate taxes for applicable assets, e.g. mutual funds, stocks, etc.
    * note, this means capital gains taxes aren't implemented
* handle all start dates correctly, e.g. if the sim start date is after an asset's start date, the asset needs to appreciate before the sim starts
"""
function simulateRetirement(portfolio::RetirementPortfolio; start_date=Dates.today(), samples=10000)
    asset_names = collect(keys(portfolio.investments))
    append!(asset_names, collect(keys(portfolio.accounts)))
    if !validateExpenseToAssetMapping(portfolio.expenses2assets, keys(portfolio.expenses), asset_names)
        error("Incorrect mapping of expenses to assets. Please double-check $(portfolio.expenses2assets).")
    end

    birthday = portfolio.profile.birthdate
    retirement_age = portfolio.profile.retirement_age
    life_expectancy = portfolio.profile.life_expectancy
    retirement_date = birthday + Dates.Year(retirement_age)

    num_working_years = round(retirement_age - (Dates.year(start_date) - Dates.year(birthday)))
    num_years = round(life_expectancy - (Dates.year(start_date) - Dates.year(birthday)))

    # Initialise variables
    market_returns = Dict{String,Matrix{Float64}}()
    # matrix rows are years, columns are samples
    assets = Dict{String,Matrix{Float64}}()
    liabilities = Dict{String,Vector{Float64}}()
    expenses = Dict{String,Vector{Float64}}()
    for key ∈ keys(portfolio.expenses)
        expenses[key] = zeros(num_years)
    end
    net_worth = zeros(num_years, samples)
    total_assets = zeros(samples)
    salary = zeros(num_years)
    savings = zeros(num_years)
    for (key, investment) ∈ portfolio.investments
        market_returns[key] = rand(investment.fund, num_years, samples)
        zf = zeros(num_years, samples)
        # TODO: calculate possible return for principal based on years into the future
        # if the fund was started in 1990 but today is 2019, the principal should not be the same
        zf[1, :] .= investment.principal
        assets[key] = zf
        total_assets .+= zf[1, :]
    end
    net_worth[1, :] = total_assets

    for iYear = 1:num_years
        cur_date = start_date + Dates.Year(iYear)
        cur_salary = cur_date < retirement_date ? calcAnnualSalary(portfolio.salary, date=cur_date) : 0
        salary[iYear] = cur_salary

        # Get all expenses which are paid for by salary
        salary_expenses = 0
        for (key, expense) ∈ portfolio.expenses
            temp_cost = estimateAnnualExpenses(expense, cur_date)
            if isnothing(portfolio.expenses2assets) || key ∉ keys(portfolio.expenses2assets)
                salary_expenses += temp_cost
            end
            expenses[key][iYear] = temp_cost
        end

        # Calculate post-tax income, then subtract relevant expenses
        post_tax_income = calcAnnualSalary(portfolio.salary, date=cur_date, estimate_tax=true, print_debug=false)
        savings_amount = post_tax_income + salary_expenses
        if savings_amount < 0 && cur_date < retirement_date
            error("Not enough income ($post_tax_income) to support expenses ($salary_expenses).")
        end
        # Salary only valid if you're still working
        savings[iYear] = cur_date < retirement_date ? savings_amount : 0.0

        # Monte-Carlo for asset returns
        total_assets = zeros(samples)
        for (key, investment) ∈ portfolio.investments
            market_return = market_returns[key][iYear, :]
            prev_return = iYear > 1 ? assets[key][iYear - 1, :] : assets[key][iYear, :]
            annual_return = (1 .+ market_return) .* prev_return
            # if not retired, contribute to reach investment equally
            # otherwise, pay for expenses out of all accounts equally
            if cur_date < retirement_date
                # TODO: allow for user-defined mapping of savings to investments
                annual_return .+= savings_amount / length(portfolio.investments)
            else
                annual_return .+= salary_expenses / length(portfolio.investments)
            end
            assets[key][iYear, :] = annual_return
            total_assets += assets[key][iYear, :]
        end
        net_worth[iYear, :] = total_assets

        # Withdraw remaining expenses from the relevant accounts
        if !isnothing(portfolio.expenses2assets)
            for (ek, ak) ∈ portfolio.expenses2assets
                if ak ∈ keys(assets)
                    assets[ak][iYear,:] .+= expenses[ek][iYear]
                elseif ak ∈ keys(portfolio.accounts)
                    error("Accounts aren't supported yet for withdrawing expenses.")
                else
                    error("You shouldn't get here! Something went wrong when trying to map expenses to accounts.")
                end
                net_worth[iYear,:] .+= expenses[ek][iYear]
            end
        end
    end
    return RetirementSimOutput(portfolio, start_date, salary, expenses, savings, assets, liabilities, net_worth)
end