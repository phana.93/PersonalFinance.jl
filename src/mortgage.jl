
#= 
Copyright [2020] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#

"""
    FixedRateMortgage(principal, down_payment, annual_rate, loan_term)

* `principal::Float64`: original listing price
* `down_payment::Float64`: down payment, expressed as a decimal on [0, 1]
* `annual_rate::Float64`: nominal annual rate, expressed as a decimal on [0, 1]
* `loan_term::Dates.Month`: loan term in months
    * Optionally, you can pass in `Dates.Year`
"""
struct FixedRateMortgage
    principal::Float64
    down_payment::Float64
    annual_rate::Float64
    loan_term::Dates.Month
end

FixedRateMortgage(P::Real, d::Real, r::Real, N::Dates.Month) = FixedRateMortgage(Float64(P), Float64(d), Float64(r), N)
FixedRateMortgage(P::Real, d::Real, r::Real, N::Dates.Year) = FixedRateMortgage(Float64(P), Float64(d), Float64(r), Dates.Month(N.value * 12))

"""
    calculateFixedRateMortgagePayment(m::FixedRateMortgage)

Returns a tuple of `(monthly_payment, total_cost)` based on a [`FixedRateMortgage`](@ref).
References:
* [Wikipedia](https://en.wikipedia.org/wiki/Mortgage_calculator)
* [Bankrate](https://www.bankrate.com/calculators/mortgages/amortization-calculator.aspx)
"""
function calculateFixedRateMortgagePayment(P, down_payment_rate, annual_rate, loan_term::Dates.Month)
    r = annual_rate / 12
    N = loan_term.value
    monthly_payment = r * (P * (1 - down_payment_rate)) / (1 - (1 + r)^(-N))
    total_cost = monthly_payment * N + P * down_payment_rate
    return monthly_payment, total_cost
end

function calculateFixedRateMortgagePayment(m::FixedRateMortgage)
    return calculateFixedRateMortgagePayment(m.principal, m.down_payment, m.annual_rate, m.loan_term)
end

"""
    calculateAmountOwed(mortgage::FixedRateMortgage, t)

Calculate the amount remaining that is still owed on the mortgage at the end of month `t`.
* `mortgage`: see [`FixedRateMortgage`](@ref)
* `t::Int`: month

References:
* [Wikipedia](https://en.wikipedia.org/wiki/Mortgage_calculator)
"""
function calculateAmountOwed(mortgage::FixedRateMortgage, monthly_payment, t)
    P = mortgage.principal * (1 - mortgage.down_payment)
    r = mortgage.annual_rate / 12
    due = (1 + r)^t * P - ((1 + r)^t - 1) / r * monthly_payment
end

function calculateAmountOwed(mortgage::FixedRateMortgage, t)
    monthly_payment, _ = calculateFixedRateMortgagePayment(mortgage)
    return calculateAmountOwed(mortgage, monthly_payment, t)
end

"""
    FixedRateMortgageSchedule

A struct that represents the amortization schedule for a [`FixedRateMortgage`](@ref):
* `m::FixedRateMortgage`: the input mortgage used to generate these outputs
* `total_cost`: total cost of the mortgage over its lifetime
* `total_interest`: the total interest of the mortgage over its lifetime
* `monthly_payment`: the monthly mortgage payment
* `principal_payments, interest_payments, balance_remaining`: the amounts per year for principal and interest payments, as well as the balance remaining
"""
struct FixedRateMortgageSchedule
    m::FixedRateMortgage
    total_cost::Float64
    total_interest::Float64
    monthly_payment::Float64
    principal_payments::Vector{Float64}
    interest_payments::Vector{Float64}
    balance_remaining::Vector{Float64}
end

"""
    calculateFixedRateMortgageSchedule(mortgage::FixedRateMortgage)

Calculate the amortization schedule for a [`FixedRateMortgage`](@ref).
"""
function calculateFixedRateMortgageSchedule(mortgage::FixedRateMortgage)
    monthly_payment, total_cost = calculateFixedRateMortgagePayment(mortgage)
    months = 1:mortgage.loan_term.value
    principal_payments = Vector{Float64}(undef, mortgage.loan_term.value)
    interest_payments = Vector{Float64}(undef, mortgage.loan_term.value)
    balance_remaining = Vector{Float64}(undef, mortgage.loan_term.value)
    r = mortgage.annual_rate / 12
    for month ∈ months
        amount_owed = calculateAmountOwed(mortgage, monthly_payment, month)
        prev_month_balance = month == 1 ? mortgage.principal * (1 - mortgage.down_payment) : balance_remaining[month - 1]
        balance_remaining[month] = amount_owed
        interest_payments[month] = prev_month_balance * r
        principal_payments[month] = monthly_payment - interest_payments[month]
    end
    total_interest = total_cost - mortgage.principal
    return FixedRateMortgageSchedule(mortgage, total_cost, total_interest, monthly_payment, principal_payments, interest_payments, balance_remaining)
end

"""
    HousePayment(m, property_tax, insurance)

* `m`: [`FixedRateMortgage`](@ref)
* `property_tax::Float64`: property tax expressed as a decimal on [0, 1]
* `insurance::Float64`: insurance expressed as a decimal on [0, 1]
"""
struct HousePayment
    m::FixedRateMortgage
    property_tax::Float64
    insurance::Float64
end

"""
    HousePaymentSchedule

A struct for outputting:
* `mortgage_schedule::FixedRateMortgageSchedule`: see [`FixedRateMortgageSchedule`](@ref)
* `property_taxes::Float64`: monthly payment for property taxes
* `insurance_payment::Float64`: monthly insurance payment
* `total::Float64`: monthly total for all housing-related payments
"""
struct HousePaymentSchedule
    mortgage_schedule::FixedRateMortgageSchedule
    property_taxes::Float64
    insurance_payment::Float64
    total::Float64
end

"""
    calculateMonthlyPayments(h::HousePayment)

Given a [`HousePayment`](@ref), calculate monthly payments including the mortgage (see [`calculateFixedRateMortgageSchedule`](@ref)), property tax, and homeowners insurance.
"""
function calculateMonthlyPayments(h::HousePayment)
    m = calculateFixedRateMortgageSchedule(h.m)
    pt = h.property_tax * h.m.principal / 12
    hi = h.insurance * h.m.principal / 12
    total = m.monthly_payment + pt + hi
    return HousePaymentSchedule(m, pt, hi, total)
end

# Home appreciation references:
# https://www.laalmanac.com/economy/ec37.php
# https://fred.stlouisfed.org/series/CASTHPI
# https://www.realestateabc.com/graphs/calmedian.html
# https://dqydj.com/historical-home-prices/
# https://car.sharefile.com/share/view/s0c02663a5c54e23a