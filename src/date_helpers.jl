#= 
Copyright [2020] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#

function calcNumPeriodsPerYear(period::Dates.Period)
    if typeof(period) == Dates.Week
        return 52 / Dates.value(period)
    elseif typeof(period) == Dates.Day
        return 364 / Dates.value(period)
    elseif typeof(period) == Dates.Month
        return 12 / Dates.value(period)
    else
        error("PersonalFinance does not support a period of $period.")
    end
end

"""
    calcDaysOfOcurrence(period, event_start, period_start, period_end)

Given period and start date of an event, sample days that the event occurs within
a period (inclusive).
"""
function calcDaysOfOcurrence(period, event_start, period_start, period_end)
    # tmp_dates = event_start:period:period_end
    # return tmp_dates[ tmp_dates .>= period_start]
    return findOccurrences(event_start, period, Pair(period_start, period_end))
end

"""
    findOccurrences(event_start, event_period, sample_range)

Given the start of an event and its period, find the days the event occurs:
* `event_start`: start date of the event
* `event_period`: period (i.e. 1/frequency) of the event
* `sample_range`: a tuple of [start, end] dates to sample dates of occurrences (inclusive)

Given period and start date of an event, sample days that the event occurs within
a period (inclusive).
"""
function findOccurrences(event_start, event_period, sample_range)
    tmp_dates = event_start:event_period:sample_range.second
    return tmp_dates[ tmp_dates .>= sample_range.first]
end

@deprecate calcDaysOfOcurrence findOccurrences false
