#= 
Copyright [2020] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#

"""
    PersonalProfile(first_name, last_name, birthdate)
    PersonalProfile(first_name, middle_name, last_name, birthdate)
    PersonalProfile(first_name, last_name, birthdate, retirement_age, life_expectancy)
    PersonalProfile(first_name, middle_name, last_name, birthdate, retirement_age, life_expectancy)
"""
mutable struct PersonalProfile <: AbstractProfile
    first_name::String
    middle_name::String
    last_name::String
    birthdate::Dates.Date
    retirement_age::Real
    life_expectancy::Real

    PersonalProfile(first_name, middle_name, last_name, birthdate, retirement_age, life_expectancy) =
        ((retirement_age > life_expectancy) || (retirement_age < 0) || (life_expectancy < 0)) ?
        error("Inputs must be positive. Retirement age should be less than life expectancy.") :
        new(first_name, middle_name, last_name, birthdate, retirement_age, life_expectancy)
end

#### Outer constructors
PersonalProfile(first_name, last_name, birthdate) =
    PersonalProfile(first_name, "", last_name, birthdate, 70, 90)
PersonalProfile(first_name, middle_name, last_name, birthdate) =
    PersonalProfile(first_name, middle_name, last_name, birthdate, 70, 90)
PersonalProfile(first_name, last_name, birthdate, retirement_age, life_expectancy) =
    PersonalProfile(first_name, "", last_name, birthdate, retirement_age, life_expectancy)
