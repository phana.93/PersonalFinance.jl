#= 
Copyright [2020] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#

function summary(portfolio::SimplePortfolio; state="CA", start_date=Dates.today())
    annual_tax = calcTax(portfolio.salary.annual_salary, state, year(start_date))
    annual_di = portfolio.salary.annual_salary - annual_tax

    monthly_salary = (portfolio.salary.annual_salary - annual_tax) / 12

    annual_costs = sumAnnualExpenses(portfolio)
    monthly_costs = annual_costs / 12
    monthly_savings = monthly_salary + monthly_costs
    annual_savings = annual_di + annual_costs
    paycheck = calcPaycheck(portfolio.salary, estimate_tax=true, state=state)

    federal_tax = calcFederalTax(portfolio.salary.annual_salary, year(start_date))
    ss_tax = calcSSTax(portfolio.salary.annual_salary, year(start_date))
    medicare_tax = calcMedicareTax(portfolio.salary.annual_salary, year(start_date))
    state_tax = calcStateTax(portfolio.salary.annual_salary, state, year(start_date))

    disp_str =
    "--------------
Annual Summary
--------------
Salary: \$$(round(portfolio.salary.annual_salary, digits=2))
Total tax: \$$(round(-annual_tax, digits=2))

Disposable income: \$$(round(portfolio.salary.annual_salary - annual_tax, digits=2))
Costs: \$$(round(annual_costs, digits=2))
Expected savings: \$$(round(annual_savings, digits=2))

--------------------------
Monthly Summary (post-tax)
--------------------------
Average income (per month): \$$(round(monthly_salary, digits=2))
Paycheck (per payperiod): \$$(round(paycheck, digits=2))
Average costs: \$$(round(monthly_costs, digits=2))
Average savings: \$$(round(monthly_savings, digits=2))

-------------
Tax Breakdown
-------------
Federal tax: \$$(round(federal_tax, digits=2))
SS tax: \$$(round(ss_tax, digits=2))
Medicare tax: \$$(round(medicare_tax, digits=2))
State tax for $state: \$$(round(state_tax, digits=2))

-----------------
Savings Breakdown
-----------------
Savings rate, pre-tax: $(round(annual_savings / portfolio.salary.annual_salary * 100, digits=2))%
Savings rate, post-tax: $(round(annual_savings / annual_di * 100, digits=2))%"
    println(disp_str)
end

function summary(portfolio::RetirementPortfolio; display_plots=true, samples=10000)
    simple_portfolio = SimplePortfolio(portfolio.salary, portfolio.accounts, portfolio.expenses)
    summary(simple_portfolio)

    results = simulateRetirement(portfolio, samples=samples)
    plotRetirement(results, display_plots=display_plots)

    final_net_worth = results.net_worth[end,:]
    mean_net_worth = Statistics.mean(final_net_worth)
    median_net_worth = Statistics.median(final_net_worth)
    total_savings = sum(results.savings)
    total_salary = sum(results.salary)
    savings_rate = round(total_savings / total_salary * 100, digits=2)

    disp_str =
"------------------------------
Summary of Retirement Accounts
------------------------------
Estimated net worth at retirement (mean): \$$mean_net_worth
Estimated net worth at retirement (median): \$$median_net_worth

Direct savings: \$$total_savings
Average savings rate, pre-tax: $savings_rate%
"
    println(disp_str)
end

function summary(h::HousePaymentSchedule)
    pi = round(h.mortgage_schedule.monthly_payment, digits=2)
    pt = round(h.property_taxes, digits=2)
    hi = round(h.insurance_payment, digits=2)
    total = round(h.total, digits=2)
    disp_str =
"---------------------------------
Summary of Monthly House Payments
---------------------------------
Mortgage = \$$pi
Property taxes = \$$pt
Home insurance = \$$hi

Total = \$$total
"
    println(disp_str)
end