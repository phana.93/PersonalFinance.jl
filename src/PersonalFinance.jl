#= 
Copyright [2020] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#

module PersonalFinance

using Reexport

@reexport using Distributions
@reexport using Statistics
@reexport using StatsBase
@reexport using Dates
@reexport using Random
@reexport using PlotlyJS

import Colors
import Nord

include("abstract_types.jl")

include("date_helpers.jl")

include("salary.jl")
include("tax/tax.jl")
include("expenses.jl")
include("profile.jl")
include("asset.jl")
include("portfolio.jl")
include("mortgage.jl")
include("sim.jl")
include("summary.jl")
include("plots.jl")

"""
    greet()

Welcome the user to PersonalFinance.jl.
"""
greet() = println("Welcome to PersonalFinance.jl.")

"""
    dir()

Return the directory of PersonalFinance.
"""
dir() = abspath(joinpath(@__DIR__, ".."))

end # end PersonalFinance
