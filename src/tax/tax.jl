#= 
Copyright [2020] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#

"""
    FilingStatus

Enum for filing status:
* single = 1
* married_joint = 2
* married_separate = 3
* head_of_household = 4
* widow = 5
"""
module FilingStatus
    filing_status_type = Int
    single = 1
    married_joint = 2
    married_separate = 3
    head_of_household = 4
    widow = 5
end

############
# INCLUDES #
############
include("federal_tax.jl")
include("state_tax.jl")
############
# INCLUDES #
############

"""
    hasTax(state, year)

Helper function to check if PersonalFinance has enough information to calculate taxes for the state & year provided.
"""
hasTax(state, year, filing_status=FilingStatus.single) = hasFederalTax(year, filing_status) && hasFICATax(year) && hasStateTax(state, year)

"""
    getValidTaxConfiguration

Helper function which returns a valid tax configuration.
"""
function getValidTaxConfiguration()
    state = "CA"
    year = 2020
    @assert hasTax(state, year)
    return state, year
end

"""
    calcTax(salary, state, year)

`salary::Float64`: dollar amount of salary
`state::String`: full state name or acronym
`year::Int`: year of tax calculation

Helper function to calculate tax for a single taxpayer, which includes
federal tax, FICA tax, and state tax. Assumptions include:
    * status is single
    * no dependents
    * using standard deduction
    * any other simplifying assumption you can think of
"""
function calcTax(salary::Float64, state::String, year::Int)
    tax_owed = calcFederalTax(salary, year)
    tax_owed += calcFICATax(salary, year)
    tax_owed += calcStateTax(salary, state, year)
    return tax_owed
end
