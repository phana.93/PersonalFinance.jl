#= 
Copyright [2020] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#

# year -> tax table
tax_table_type = Dict{Int,Matrix{Float64}}
standard_deduction_type = Dict{Int,Float64}

state_tax = Dict{String,tax_table_type}()
state_deduction = Dict{String,standard_deduction_type}()

ca_single_tax = tax_table_type()
ca_standard_deduction = standard_deduction_type()

ca_single_tax_2019 = [0.01 8809;
                      0.02 20883;
                      0.04 32960;
                      0.06 45753;
                      0.08 57824;
                      0.093 295373;
                      0.103 354445;
                      0.113 590742;
                      0.123 Inf]
ca_single_tax[2019] = ca_single_tax_2019
ca_standard_deduction[2019] = 4537

ca_single_tax_2018 = [0.01 8544;
                      0.02 20255;
                      0.04 31969;
                      0.06 44377;
                      0.08 56085;
                      0.093 286492;
                      0.103 343788;
                      0.113 572980;
                      0.123 Inf]
ca_single_tax[2018] = ca_single_tax_2018
ca_standard_deduction[2018] = 4401

ca_single_tax_2020 = ca_single_tax_2019
ca_standard_deduction[2020] = ca_standard_deduction[2019]
ca_single_tax[2020] = ca_single_tax_2020

state_tax["CA"] = ca_single_tax
state_tax["California"] = state_tax["CA"]
state_deduction["CA"] = ca_standard_deduction
state_deduction["California"] = state_deduction["CA"]

"""
    hasStateTax(state::String, year::Int)

`state` can be the full state name or acronym.
"""
function hasStateTax(state, year)
    if !(haskey(state_tax, state) && haskey(state_deduction, state))
        return false
    end
    my_state_tax = state_tax[state]
    my_state_deduction = state_deduction[state]
    if !(haskey(my_state_deduction, year) && haskey(my_state_tax, year))
        return false
    end
    return true
end

"""
    calcStateTax(salary::Float64, state::String, year::Int)

Computes tax for the given state and year. Assumes simple standard deduction applies. `state` can be the full state name or acronym.
"""
function calcStateTax(salary::Float64, state::String, year::Int)
    if !haskey(state_tax, state)
        throw(ArgumentError("PersonalFinance's state tax bracket doesn't contain the state $state."))
    end
    if !haskey(state_deduction, state)
        throw(ArgumentError("PersonalFinance's standard deduction table for states doesn't contain the state $state."))
    end
    my_state_tax = state_tax[state]
    my_state_deduction = state_deduction[state]
    if !haskey(my_state_tax, year)
        throw(ArgumentError("PersonalFinance's $state tax bracket doesn't contain the year $year."))
    end
    if !haskey(my_state_deduction, year)
        throw(ArgumentError("PersonalFinance's $state tax deduction doesn't contain the year $year."))
    end
    tax_table = my_state_tax[year]
    taxable_income = salary - my_state_deduction[year]
    num_rows, _ = size(tax_table)
    if taxable_income <= 0.0
        return 0.0
    end
    tax_owed = 0.0
    for i = 1:num_rows
        rate = tax_table[i, 1]
        limit = tax_table[i, 2]
        lower_limit = i == 1 ? 0 : tax_table[i - 1, 2]
        if taxable_income < limit
            tax_owed += rate * (taxable_income - lower_limit)
            break
        elseif taxable_income > limit
            tax_owed += rate * (limit - lower_limit)
        end
    end
    return tax_owed
end

calcStateTax(salary::Real, state::String, year::Int) = calcStateTax(Float64(salary), state, year)
