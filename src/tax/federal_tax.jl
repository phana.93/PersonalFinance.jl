#= 
Copyright [2020] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#



# year -> tax table
TaxTable = Dict{Int,Matrix{Float64}}
federal_single_tax = TaxTable()

federal_tax = Dict{FilingStatus.filing_status_type,TaxTable}()
federal_tax[FilingStatus.single] = federal_single_tax

DeductionTable = Dict{Int,Float64}
single_standard_deduction = DeductionTable()

# col0: rate
# col1: income
# Example: row 1 denotes 10% tax on income up to 9875
single_tax_2020 = [0.10 9875;
                   0.12 40125;
                   0.22 85525;
                   0.24 163300;
                   0.32 207350;
                   0.35 518400;
                   0.37 Inf]
federal_single_tax[2020] = single_tax_2020
single_standard_deduction[2020] = 12400

single_tax_2019 = [0.10 9700;
                   0.12 39475;
                   0.22 84200;
                   0.24 160725;
                   0.32 204100;
                   0.35 510300;
                   0.37 Inf]
federal_single_tax[2019] = single_tax_2019
single_standard_deduction[2019] = 12200

single_tax_2018 = [0.10 9525;
                   0.12 38700;
                   0.22 82500;
                   0.24 157500;
                   0.32 200000;
                   0.35 500000;
                   0.37 Inf]
federal_single_tax[2018] = single_tax_2018
single_standard_deduction[2018] = 12000

function calcFederalTax(salary::SimpleSalary, year::Int)
    return calcFederalTax(salary.annual_salary, year)
end

"""
    hasFederalTax(year::Int, filing_status::Int)

Returns `true` if this package contains federal tax brackets for the given year and [`filing status`](@ref FilingStatus)
"""
function hasFederalTax(year, filing_status=FilingStatus.single)
    return haskey(federal_tax, filing_status) && haskey(federal_tax[filing_status], year)
end

"""
    calcFederalTax(salary::Float64, year::Int, filing_status::Int)

Calculates federal tax for the salary in the given year, assuming:
    * status is single
    * no dependents
    * standard deduction is applied
"""
function calcFederalTax(salary::Float64, year::Int, filing_status::Int=FilingStatus.single)
    if !hasFederalTax(year, filing_status)
        throw(ArgumentError("PersonalFinance's federal tax calculation doesn't support the year $year and/or the filing status of $filing_status."))
    end

    tax_table = federal_tax[filing_status][year]
    taxable_income = salary - single_standard_deduction[year]
    if taxable_income <= 0.0
        return 0.0
    end
    num_rows, _ = size(tax_table)
    tax_owed = 0.0
    for i = 1:num_rows
        rate = tax_table[i, 1]
        limit = tax_table[i, 2]
        lower_limit = i == 1 ? 0 : tax_table[i - 1, 2]
        if taxable_income < limit
            tax_owed += rate * (taxable_income - lower_limit)
            break
        elseif taxable_income > limit
            tax_owed += rate * (limit - lower_limit)
        end
    end
    return tax_owed
end
calcFederalTax(salary::Real, year::Int) = calcFederalTax(Float64(salary), year)

ss_tax = Dict{Int,Dict{String,Float64}}()
ss_tax_2020 = Dict{String,Float64}()
ss_tax_2020["rate"] = 0.062
ss_tax_2020["limit"] = 137700
ss_tax[2020] = ss_tax_2020
ss_tax_2019 = Dict{String,Float64}()
ss_tax_2019["rate"] = 0.062
ss_tax_2019["limit"] = 132900
ss_tax[2019] = ss_tax_2019

medicare_tax = Dict{Int,Dict{String,Float64}}()
medicare_tax[2019] = Dict{String,Float64}()
medicare_tax[2019]["base_rate"] = 0.0145
medicare_tax[2019]["limit"] = 200000
medicare_tax[2019]["excess_rate"] = 0.009
medicare_tax[2020] = Dict{String,Float64}()
medicare_tax[2020]["base_rate"] = 0.0145
medicare_tax[2020]["limit"] = 200000
medicare_tax[2020]["excess_rate"] = 0.009

"""
    calcSSTax(salary::Float64, year::Int)

Calculates social security tax.
"""
function calcSSTax(salary::Float64, year::Int)
    if !haskey(ss_tax, year)
        throw(ArgumentError("PersonalFinance's social security table doesn't contain the year $year."))
    end

    if salary < ss_tax[year]["limit"]
        tax_owed = salary * ss_tax[year]["rate"]
    else
        tax_owed = ss_tax[year]["limit"] * ss_tax[year]["rate"]
    end

    return tax_owed
end
calcSSTax(salary::Real, year::Int) = calcSSTax(Float64(salary), year)

"""
    calcMedicareTax(salary::Float64, year::Int)

Calculates medicare tax.
"""
function calcMedicareTax(salary::Float64, year::Int)
    if !haskey(medicare_tax, year)
        throw(ArgumentError("PersonalFinance's medicare tax table doesn't contain the year $year."))
    end
    base_tax = salary * medicare_tax[year]["base_rate"]
    if salary <= medicare_tax[year]["limit"]
        return base_tax
    end
    return base_tax + (salary - medicare_tax[year]["limit"]) * medicare_tax[year]["excess_rate"]
end
calcMedicareTax(salary::Real, year::Int) = calcMedicareTax(Float64(salary), year)

"""
    hasFICATax(year::Int)
"""
hasFICATax(year) = haskey(ss_tax, year) && haskey(medicare_tax, year)

"""
    calcFICATax(salary::Real, year::Int)

Calculates FICA tax, e.g. social security & medicare.
"""
function calcFICATax(salary::Real, year::Int)
    return calcSSTax(salary, year) + calcMedicareTax(salary, year)
end
